Installing SysV

1. Packages
You'll need 3 AUR packages:
'
sysvinit
initscripts-fork
consolekit
'
And 'xorg-xdm' normal package

First use 'pacman -Rdd --noconfirm systemd-sysvcompat polkit' to clean system from systemd,
now install sysvinit, initscripts-fork and consolekit with yaourt. After doing this install xorg-xdm
package, and enable xdm daemon in /etc/rc.conf

Reboot and Done

2. Xorg
xdm won't work if you don't do this.
-place run commands in .xsession
-run 'chmod 700 ~/.xsession' done and reboot
Note to desktop environments! Instead of using 'startlxde' (for example) to start X
use 'ck-launch-session dbus-launch startlxde'. You don't have to do this if using window manager.

3. inittab
Before you reboot you have to fetch inittab to /etc:
run 'curl -L -o /etc/inittab https://codeberg.org/glowiak/sysvarch/raw/branch/master/inittab" as root
if inittab exists use 'rm -rf /etc/inittab' and fetch it again